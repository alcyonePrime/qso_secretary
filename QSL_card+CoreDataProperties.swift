//
//  QSL_card+CoreDataProperties.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-29.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData


extension QSL_card {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<QSL_card> {
        return NSFetchRequest<QSL_card>(entityName: "QSL_card")
    }

    @NSManaged public var qsl_rcvd: String?
    @NSManaged public var qsl_sent: String?
    @NSManaged public var clublog_qso_upload_status: String?
    @NSManaged public var hrdlog_qso_upload_status: String?
    @NSManaged public var lotw_qsl_rcvd: String?
    @NSManaged public var lotw_qsl_sent: String?
    @NSManaged public var eqsl_qsl_sent: String?
    @NSManaged public var eqsl_qsl_rcvd: String?
    @NSManaged public var qslmsg: String?
    @NSManaged public var qrzcom_qso_upload_status: String?
    @NSManaged public var notes: String?
    @NSManaged public var qsl_sent_via: String?
    @NSManaged public var qsl_rcvd_via: String?
    @NSManaged public var lotw_qsl_rcvd_date: String?
    @NSManaged public var lotw_qsl_sent_date: String?
    @NSManaged public var qso_info: QSO?

}
