//
//  QsoTableViewCell.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 21/09/16.
//  Copyright © 2016 Natalia Mykhailovska. All rights reserved.
//

import UIKit
import CoreData

class QsoTableViewCell: UITableViewCell {
    // MARK: Properties
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var callsignLabel: UILabel!
    @IBOutlet weak var bandLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
   
    static let reuseIdentifier = "QSOCell"
    
    var cellId: NSManagedObjectID?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(record: NSManagedObject) {
        cellId = record.objectID
        // configure the labels, etc in the cell
    }

}
