//
//  Flux_ind+CoreDataProperties.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData


extension Flux_ind {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Flux_ind> {
        return NSFetchRequest<Flux_ind>(entityName: "Flux_ind")
    }

    @NSManaged public var a_index: NSNumber?
    @NSManaged public var notes: String?
    @NSManaged public var sfi: NSNumber?
    @NSManaged public var k_index: NSNumber?

}
