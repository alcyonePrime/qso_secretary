//
//  Aliases+CoreDataClass.swift
//  QSO Secretary
//
//  Created by Andrew Valender on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Aliases)
public class Aliases: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Aliases"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
    var prefDBinfo: String?
    var parent: String!
    var country:String!
    var continent: String!
    var cq: String!
    var itu:String!
    var lokator: String!
    var stolica: String!
    var gmt:String!
    var hamatlas:String!
    
    func getCallbookRecord(call:String){
        
        if (call.count > 1)
        {
            var fetchedRecord:[NSManagedObject]
            let managedContext = CoreDataManager.instance.managedObjectContext
            let recordsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Aliases")
            
            var index = call.index(call.startIndex, offsetBy: 3)
            var alias = String(call[...index])
            
            recordsFetch.predicate = NSPredicate(format: "alias == %@", alias)
            do {
                fetchedRecord = try managedContext.fetch(recordsFetch) as! [NSManagedObject]
                //print ( "count1: \(fetchedEmployees.count), alias: \(alias)")
                if fetchedRecord.count == 0 {
                    index = call.index(call.startIndex, offsetBy: 2)
                    alias = String(call[...index])
                    recordsFetch.predicate = NSPredicate(format: "alias == %@", alias)
                    fetchedRecord = try managedContext.fetch(recordsFetch) as! [NSManagedObject]
                }
                
                for record in fetchedRecord {
                    let c_book = Callbook()
                    c_book.getParentRecord(dxccEntity: record.value(forKey: "dxcc_pref") as! String)
                    prefDBinfo = record.value(forKey: "about") as? String
                    country = c_book.prefDBcountry
                    continent = c_book.prefDBcontinent
                    cq = c_book.prefDBcq
                    itu = c_book.prefDBitu
                    lokator = c_book.prefDBlokator
                    stolica = c_book.prefDBstolica
                    gmt = c_book.prefDBgmt
                }
            } catch {
                fatalError("Failed to fetch callbook: \(error)")
            }
        }
    }
}
