//
//  Designators+CoreDataClass.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Designators)
public class Designators: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Designators"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
}
