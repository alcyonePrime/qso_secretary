//
//  My_info+CoreDataProperties.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData


extension My_info {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<My_info> {
        return NSFetchRequest<My_info>(entityName: "My_info")
    }

    @NSManaged public var gridsquare: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?
    @NSManaged public var name: String?
    @NSManaged public var equipment: String?
    @NSManaged public var oper: String?
    @NSManaged public var sig: String?
    @NSManaged public var city: String?
    @NSManaged public var station_callsign: String?
    @NSManaged public var references: String?
    @NSManaged public var notes: String?

}
