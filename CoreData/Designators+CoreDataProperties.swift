//
//  Designators+CoreDataProperties.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData


extension Designators {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Designators> {
        return NSFetchRequest<Designators>(entityName: "Designators")
    }

    @NSManaged public var iota_ref: String?
    @NSManaged public var notes: String?
    @NSManaged public var gridsquare: String?
    @NSManaged public var sota_ref: String?
    @NSManaged public var rda_ref: String?
    @NSManaged public var qso_info: QSO?

}
