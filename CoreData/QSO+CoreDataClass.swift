//
//  QSO+CoreDataClass.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData

@objc(QSO)
public class QSO: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "QSO"), insertInto: CoreDataManager.instance.managedObjectContext)
        
        let designator:Designators = Designators()
        self.designators = designator
        
        let cards:QSL_card = QSL_card()
        self.cards = cards
    }
    
    static let allModes = ["CW", "AM", "FM", "SSB", "RTTY", "PSK31", "PSK63", "DIGITALVOICE", "DMR", "DSTAR", "FSK441", "FT8", "JT65", "JT6M"]
    
    static let bandsFreqs = [
        ["2190m",".136"],
        ["560m",".501"],
        ["160m","1.8"],
        ["80m","3.5"],
        ["60m","5.102"],
        ["40m","7.0"],
        ["30m","10.0"],
        ["20m","14.0"],
        ["17m","18.068"],
        ["15m","21.0"],
        ["12m","24.890"],
        ["10m","28.0"],
        ["6m","50"],
        ["4m","70"],
        ["2m","144"],
        ["1.25m","222"],
        ["70cm","432"],
        ["33cm","902"],
        ["23cm","1,240"],
        ["13cm","2,300"],
        ["9cm","3,300"],
        ["6cm","5,650"],
        ["3cm","10,000"],
        ["1.25cm","24,000"],
        ["6mm","47,000"],
        ["4mm","75,500"],
        ["2.5mm","119,980"],
        ["2mm","142,000"],
        ["1mm","241,000"]]
    
    static let allSatelites = [ "None", "AO-7", "AO-73", "AO-85", "AO-91", "AO-92", "EO-79", "FO-29", "ISS-DATA", "ISS-FM", "LilacSat-2", "NO-84", "SO-50", "XW-2A", "XW-2B", "XW-2C", "XW-2D", "XW-2E", "XW-2F"]
    
    static let allPropagation = [
        ["None","None"],
        ["Aurora","AUR"],
        ["Aurora-E","AUE"],
        ["Back-Scatter","BS"],
        ["EchoLink","ECH"],
        ["EME","EME"],
        ["Sporadic E","ES"],
        ["FAI", "FAI"],
        ["INET assist.","INTERNET"],
        ["Ionoscatter","ION"],
        ["IRLP","IRL"],
        ["MS","MS"],
        ["Repeater","RPT"],
        ["Rain scatter","RS"],
        ["SAT","SAT"],
        ["TEP","TEP"],
        ["Tropo","TR"],
        ["Unknown","Unknown"]]
    
    static let adifFields = [
        ["OPERATOR", "-", ""],
        ["CALL", "call", "QSO"],
        ["QSO_DATE", "date", "QSO"],
        ["TIME_ON", "time_on", "QSO"],
        ["FREQ", "frequency", "QSO"],
        ["MODE", "mode", "QSO"],
        ["SUBMODE", "submode", "QSO"],
        ["RST_SENT", "rst_sent", "QSO"],
//        ["STX", "-", ""],
        ["RST_RCVD", "rst_rcvd", "QSO"],
//        ["SRX", "-", ""],
//        ["PFX", "-", ""],
//        ["DXCC_PREF", "-", ""],
//        ["CQZ", "-", ""],
//        ["ITUZ", "-", ""],
        ["BAND", "band", "QSO"],
//        ["CONT", "-", ""],
        ["COMMENT", "comment", "QSO"],
        ["COMMENT_INTL", "comment", "QSO"],
        ["NOTES", "notes", "QSO"],
//      ["DXCC", "-", ""],
        ["GRIDSQUARE", "gridsquare", "Designators"],
        ["SAT_NAME", "sat_name", "QSO"],
        ["SAT_MODE", "sat_mode", "QSO"],
        ["PROP_MODE", "prop_mode", "QSO"],
        ["NAME", "name", "QSO"],
        ["QTH", "qth", "QSO"],
        ["IOTA", "iota_ref", "Designators"],
//        ["IOTA_ISLAND_ID", "-", ""],
        ["SOTA_REF", "sota_ref", "Designators"],
//        ["EMAIL", "-", ""],
//        ["EQSL_QSLRDATE", "-", ""],
//        ["EQSL_QSLSDATE", "-", ""],
        ["EQSL_QSL_RCVD", "eqsl_qsl_rcvd", "QSL_card"],
        ["EQSL_QSL_SENT", "eqsl_qsl_sent", "QSL_card"],
//        ["FREQ_RX", "-", ""],
        ["LOTW_QSLRDATE", "lotw_qsl_rcvd_date", "QSL_card"],
        ["LOTW_QSLSDATE", "lotw_qsl_sent_date", "QSL_card"],
        ["LOTW_QSL_RCVD", "lotw_qsl_rcvd", "QSL_card"],
        ["LOTW_QSL_SENT", "lotw_qsl_sent", "QSL_card"],
//        ["MY_GRIDSQUARE", "-", ""],
//        ["MY_IOTA", "-", ""],
//        ["MY_IOTA_ISLAND_ID", "-", ""],
//        ["MY_NAME", "-", ""],
        ["NOTES", "-", ""],  // TODO:
//        ["NOTES_INTL", "-", ""],  // TODO:
 //       ["QSLRDATE", "-", ""],
 //       ["QSLSDATE", "-", ""],
        ["QSL_RCVD", "qsl_rcvd", "QSL_card"],
        ["QSL_RCVD_VIA", "qsl_rcvd_via", "QSL_card"],
        ["QSL_SENT", "qsl_sent", "QSL_card"],
        ["QSL_SENT_VIA", "qsl_sent_via", "QSL_card"],
        ["QSLMSG", "qslmsg", "QSL_card"],
        ["QSL_VIA", "-", ""],
        ["PROGRAMID", "eQSLDownload", ""],
        ["EOR", "EOR", ""]]
    
    /*
     NA    North America
     SA    South America
     EU    Europe
     AF    Africa
     OC    Oceana
     AS    Asia
     AN    Antarctica
     */
    
    func setDefaultValues() -> Void{
        let defaults = UserDefaults.standard
        defaults.set(self.rst_sent, forKey: "lastRSTSentKey")
        defaults.set(self.rst_rcvd, forKey: "lastRSTRcvdKey")
        defaults.set(self.frequency, forKey: "lastFreqKey")
        defaults.set(self.band, forKey: "lastBandKey")
        defaults.set(self.mode, forKey: "lastModeKey")
        defaults.synchronize()
    }
    
    func getDefaultValues() -> Void{
        let defaults = UserDefaults.standard
        
        if let stringOne = defaults.string(forKey: "lastRSTSentKey") {
            self.rst_sent = stringOne // Some String Value
        } else {
            self.rst_sent = "59"
        }
        
        if let stringOne = defaults.string(forKey: "lastRSTRcvdKey") {
            self.rst_rcvd = stringOne // Some String Value
        } else {
            self.rst_rcvd = "59"
        }
        
        if let stringOne = defaults.string(forKey: "lastBandKey") {
            self.band = stringOne
        } else {
            self.band = "20m"
        }
        
        if let stringOne = defaults.string(forKey: "lastFreqKey") {
            self.frequency = stringOne
        } else {
            self.frequency = "14.0"
        }
        
        if let stringOne = defaults.string(forKey: "lastModeKey") {
            self.mode = stringOne
        } else {
            self.mode = "SSB"
        }
    }
    
    // MARK: Delete one Data Record
    func remove(_ id:NSManagedObjectID) -> Void {
        let managedContext = CoreDataManager.instance.managedObjectContext
        do {
            let record = try managedContext.existingObject(with: id)
            managedContext.delete(record)
            do {
                try managedContext.save() // <- remember to put this
            } catch {
                print("Could not save \(error)")
            }
        } catch let error as NSError {
            print("Could not fetch \(error).")
        }
    }
    
    func removeAll() -> Void{
        let managedContext = CoreDataManager.instance.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error while deleting all records")
        }
    }
    
    func loadPreviousContacts(call:String) -> Void{
        let callsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
        let fetchedCalls:[NSManagedObject]
        let managedContext = CoreDataManager.instance.managedObjectContext
        let predicate = NSPredicate(format: "call == %@", call.uppercased())
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate])
        
        callsFetch.predicate = predicateCompound
        do {
            fetchedCalls = try managedContext.fetch(callsFetch) as! [NSManagedObject]
            if fetchedCalls.count > 0 {
                self.name = fetchedCalls[0].value(forKeyPath: "name") as! String?
                self.qth = fetchedCalls[0].value(forKeyPath: "qth") as! String?
                
                self.designators?.gridsquare = fetchedCalls[0].value(forKeyPath: "gridsquare") as! String?
                self.comment = fetchedCalls[0].value(forKeyPath: "notes") as! String?
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func recordHasDupes(input:QSO) -> Bool{
        var result:Bool = false
        let fetchedCalls:[NSManagedObject]
        let managedContext = CoreDataManager.instance.managedObjectContext
        let callsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
        
        let predicate1 = NSPredicate(format: "call == %@", input.call!)
        let predicate2 = NSPredicate(format: "date == %@", input.date!)
        let predicate3 = NSPredicate(format: "time == %@", input.time_on!)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1, predicate2, predicate3])
        
        callsFetch.predicate = predicateCompound
        do {
            fetchedCalls = try managedContext.fetch(callsFetch) as! [NSManagedObject]
            
            if fetchedCalls.count > 0 {
                print("Duplicate!")
                result = true
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return result
    }
}
