//
//  Callbook+CoreDataProperties.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData


extension Callbook {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Callbook> {
        return NSFetchRequest<Callbook>(entityName: "Callbook")
    }

    @NSManaged public var continent: String?
    @NSManaged public var country: String?
    @NSManaged public var cq: String?
    @NSManaged public var dxcc_entity: String?
    @NSManaged public var dxcc_pref: String?
    @NSManaged public var gmt: String?
    @NSManaged public var hamatlas: String?
    @NSManaged public var itu: String?
    @NSManaged public var lokator: String?
    @NSManaged public var parent: String?
    @NSManaged public var stolica: String?

}
