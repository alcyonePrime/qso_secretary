//
//  QSL_card+CoreDataClass.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData

@objc(QSL_card)
public class QSL_card: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "QSL_card"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
}
