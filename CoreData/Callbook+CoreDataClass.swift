//
//  Callbook+CoreDataClass.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Callbook)
public class Callbook: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Callbook"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
    var prefDBcountry: String?
    var prefDBcontinent: String?
    var prefDBdxccEntity: String?
    var prefDBcq: String?
    var prefDBitu: String?
    var prefDBlokator: String?
    var prefDBstolica: String?
    var prefDBgmt: String?
    var prefDBhamatlas: String?
    
    func getParentRecord(dxccEntity:String){
        let fetchedEmployees:[NSManagedObject]
        let managedContext = CoreDataManager.instance.managedObjectContext
        let employeesFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Callbook")
        employeesFetch.predicate = NSPredicate(format: "parent == %@", dxccEntity)
        
        do {
            fetchedEmployees = try managedContext.fetch(employeesFetch) as! [NSManagedObject]
            for record in fetchedEmployees {
                var anOptional:String!
                //print(record.value(forKey: "country") ?? "not found")
                prefDBcountry = record.value(forKey: "country") as? String
                prefDBcontinent = record.value(forKey: "continent") as? String
                prefDBdxccEntity = record.value(forKey: "dxcc_entity") as? String
                
                anOptional = record.value(forKey: "cq") as? String
                if anOptional != nil {
                    prefDBcq = anOptional
                } else {
                    prefDBcq = "-"
                }
                
                anOptional = record.value(forKey: "itu") as? String
                if anOptional != nil {
                    prefDBitu = anOptional
                } else {
                    prefDBitu = "-"
                }
                
                prefDBlokator = record.value(forKey: "lokator") as? String
                prefDBstolica = record.value(forKey: "stolica") as? String
                prefDBgmt = record.value(forKey: "gmt") as? String
                prefDBhamatlas = record.value(forKey: "hamatlas") as? String
            }
        } catch {
            fatalError("Failed to fetch prefixes: \(error)")
        }
    }
}
