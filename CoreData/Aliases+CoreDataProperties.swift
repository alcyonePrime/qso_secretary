//
//  Aliases+CoreDataProperties.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-03.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData


extension Aliases {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Aliases> {
        return NSFetchRequest<Aliases>(entityName: "Aliases")
    }

    @NSManaged public var about: String?
    @NSManaged public var alias: String?
    @NSManaged public var dxcc_pref: String?

}
