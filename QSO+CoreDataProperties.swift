//
//  QSO+CoreDataProperties.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2019-03-29.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//
//

import Foundation
import CoreData


extension QSO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<QSO> {
        return NSFetchRequest<QSO>(entityName: "QSO")
    }

    @NSManaged public var band: String?
    @NSManaged public var call: String?
    @NSManaged public var comment: String?
    @NSManaged public var date: String?
    @NSManaged public var isExported: String?
    @NSManaged public var frequency: String?
    @NSManaged public var mode: String?
    @NSManaged public var name: String?
    @NSManaged public var notes: String?
    @NSManaged public var prop_mode: String?
    @NSManaged public var qth: String?
    @NSManaged public var rst_rcvd: String?
    @NSManaged public var rst_sent: String?
    @NSManaged public var time_on: String?
    @NSManaged public var garbage_collector: String?
    @NSManaged public var submode: String?
    @NSManaged public var sat_name: String?
    @NSManaged public var sat_mode: String?
    @NSManaged public var cards: QSL_card?
    @NSManaged public var designators: Designators?

}
