//
//  testImport.swift
//  QSO SecretaryTests
//
//  Created by Natalia Mykhailovska on 2018-09-17.
//  Copyright © 2018 Andrew Valender. All rights reserved.
//

import XCTest
@testable import QSO_Secretary

class testImport: XCTestCase {
    
    var classUnderTest: ADIFImportController!
    
    override func setUp() {
        super.setUp()
        
        classUnderTest = ADIFImportController()
        //classUnderTest.viewDidLoad()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        classUnderTest = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // XCTAssert to test model
    func testNumberOfQSOImported() {
        
        let testBundle = Bundle(for: type(of: self))
        guard let resourceURL = testBundle.url(forResource: "190_all_qso", withExtension: "adi") else {
            XCTAssert(false, "file does not exist")
            return
        }
        do {
            let resourceData = try Data(contentsOf: resourceURL)
        } catch let error {
            // some error occurred when reading the file
            XCTAssert(false, error.localizedDescription)
        }
        
        // 1. given
        classUnderTest.linkPassed = resourceURL
        let guess = 7340 //6976
        
        // 2. when
        classUnderTest.readAndParse()
       // _ = classUnderTest.check(guess: guess)
        
        // 3. then
        XCTAssertEqual(classUnderTest.qsoTotal, Int32(guess), "Score computed from guess is wrong")
        XCTAssertEqual(classUnderTest.numDuplicate, 0, "Duplicates found")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
