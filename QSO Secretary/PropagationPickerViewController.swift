//
//  PropagationPickerViewController.swift
//  QSO Secretary
//
//  Created by Andrew Valender on 05/01/17.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import Foundation
import UIKit

protocol PropagationPickerViewControllerDelegate {
    func getEnteredPropagation(data: String)
}

class PropagationPickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate  {

    var delegate: PropagationPickerViewControllerDelegate?
    var propagationValue:String!
    
    @IBOutlet weak var propagationPicker: UIPickerView!
    
    @IBAction func doneClicked(_ sender: AnyObject) {
        self.delegate?.getEnteredPropagation(data: propagationValue)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.propagationPicker.dataSource = self;
        self.propagationPicker.delegate = self;
        
      /*  if let row = QSO.allPropagation[0].index(of: propagationValue) {
            propagationPicker.selectRow(row, inComponent: 0, animated: false)
        } */
        for i in 0..<QSO.allPropagation.count {
            if (QSO.allPropagation[i][0].caseInsensitiveCompare(propagationValue) == .orderedSame) {
                propagationPicker.selectRow(i, inComponent: 0, animated: false)
                break
            }
        }
        
    }
    
    //MARK: - Delegates and data sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return QSO.allPropagation.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return QSO.allPropagation[row][0]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        propagationValue = QSO.allPropagation[row][0]
    }
    
}
