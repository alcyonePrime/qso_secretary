//
//  ModePickerViewController.swift
//  QSO Secretary
//
//  Created by Andrew Valender on 05/01/17.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import Foundation
import UIKit

protocol ModePickerViewControllerDelegate {
    func getEnteredModeValue(data: String)
}

class ModePickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate  {
    var delegate: ModePickerViewControllerDelegate?
    
    var modeValue:String!
    
    @IBOutlet weak var modePicker: UIPickerView!
    
    @IBAction func doneClicked(_ sender: AnyObject) {
        self.delegate?.getEnteredModeValue(data: modeValue)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.modePicker.dataSource = self;
        self.modePicker.delegate = self;
        
        if let row = QSO.allModes.index(of: modeValue) {
            modePicker.selectRow(row, inComponent: 0, animated: false)
        }
    }
    
    //MARK: - Delegates and data sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return QSO.allModes.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return QSO.allModes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        modeValue = QSO.allModes[row]
    }
}
