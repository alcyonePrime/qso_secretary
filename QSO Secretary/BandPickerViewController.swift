//
//  BandPickerViewController
//  QSO Secretary
//
//  Created by Andrew Valender on 05/01/17.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import Foundation
import UIKit

protocol BandPickerViewControllerDelegate {
    func getEnteredBandValue(data: String)
}

class BandPickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    var delegate: BandPickerViewControllerDelegate?
    
    var bandValue:String!
    
    @IBOutlet weak var bandPicker: UIPickerView!
    
    @IBAction func doneClicked(_ sender: AnyObject) {
        self.delegate?.getEnteredBandValue(data: bandValue)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bandPicker.dataSource = self;
        self.bandPicker.delegate = self;
        
        for i in 0..<QSO.bandsFreqs.count {
            if (QSO.bandsFreqs[i][0].caseInsensitiveCompare(bandValue) == .orderedSame) {
                bandPicker.selectRow(i, inComponent: 0, animated: false)
                break
            }
        }
    }
    
    //MARK: - Delegates and data sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return QSO.bandsFreqs.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return QSO.bandsFreqs[row][0]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        bandValue = QSO.bandsFreqs[row][0]
    }
}
