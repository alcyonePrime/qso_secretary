//
//  UIColor.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 18/10/16.
//  Copyright © 2016 Natalia Mykhailovska. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var customYellow1: UIColor {
        let darkGrey = 0xf2f5a9
        return UIColor.rgb(fromHex: darkGrey)
    }

    class var customYellow2: UIColor {
        let lightGrey = 0xf7f8e0
        return UIColor.rgb(fromHex: lightGrey)
    }
    
    class func rgb(fromHex: Int) -> UIColor {
        
        let red =   CGFloat((fromHex & 0xFF0000) >> 16) / 0xFF
        let green = CGFloat((fromHex & 0x00FF00) >> 8) / 0xFF
        let blue =  CGFloat(fromHex & 0x0000FF) / 0xFF
        let alpha = CGFloat(1.0)
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
