//
//  GridLocator.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2017-05-30.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import Foundation
import CoreLocation

class GridLocator {
    
    /// Calculate the azimuth in degrees between two locators
    /// <param name="A">Start LatLong structure</param>
    /// <param name="B">End LatLong structure</param>
    /// <returns>Azimuth in degrees</returns>
    func Azimuth(LatA:Double, LongA:Double, LatB:Double, LongB:Double) -> Float {
        let hn:Double = DegToRad(deg: LatA)
        let he:Double = DegToRad(deg: LongA)
        let n:Double = DegToRad(deg: LatB)
        let e:Double = DegToRad(deg: LongB)
        
        var co:Double = cos(he - e) * cos(hn) * cos(n) + sin(hn) * sin(n)
        var ca:Double = atan(abs(sqrt(1 - co * co) / co))
        if (co < 0){ ca = Double.pi - ca}
        
        let si:Double = sin(e - he) * cos(n) * cos(hn)
        co = sin(n) - sin(hn) * cos(ca)
        var az:Double = atan(abs(si / co))
        if (co < 0){ az = Double.pi - az}
        if (si < 0){ az = -az}
        if (az < 0){ az = az + 2 * Double.pi}
        
        return Float(az / Double.pi * 180)
    }
    
    // Convert latitude and longitude in degrees to a locator
    func  LatLongToLocator(Lat:Double, Long:Double) -> String {
        
        var locator:String = ""
        let charA:Int = 65
        let chara:Int = 97
        let char0:Int = 48
        
        var latitude:Double = Lat + 90
        var longitude:Double = Long + 180
        
        var res:Int = Int(floor(longitude / 20))
        locator += String(Character(UnicodeScalar(charA + res)!))
        
        res = Int(floor(latitude / 10))
        locator += String(Character(UnicodeScalar(charA + res)!))
        
        longitude = IEEEremainder(dividend: longitude, divisor: 20)
        if (longitude < 0) {longitude += 20}
        latitude = IEEEremainder(dividend: latitude, divisor: 10)
        if (latitude < 0) {latitude += 10}
        
        res = Int(floor(longitude / 2))
        locator += String(Character(UnicodeScalar(char0 + res)!))
        
        res = Int(floor(latitude / 1))
        locator += String(Character(UnicodeScalar(char0 + res)!))
        
        
        longitude = IEEEremainder(dividend: longitude, divisor: 2)
        if (longitude < 0) {longitude += 2}
        latitude = IEEEremainder(dividend: latitude, divisor: 1)
        if (latitude < 0) {latitude += 1}
        
        res = Int(floor(longitude * 12))
        locator += String(Character(UnicodeScalar(chara + res)!))
        
        res = Int(floor(latitude * 24))
        locator += String(Character(UnicodeScalar(chara + res)!))
        
        longitude = IEEEremainder(dividend: longitude, divisor: 1 / 12)
        if (longitude < 0) {longitude += 1 / 12}
        latitude = IEEEremainder(dividend: latitude, divisor: 1 / 24)
        if (latitude < 0) {latitude += 1 / 24}
        
        return locator
    }
    
    // Convert latitude and longitude in degrees to a locator
    func  LocatorToLatLong(locator:String) -> CLLocation{
        let charA:Double = 65
        let char0:Double = 48
        
        var coord:CLLocation = CLLocation(latitude: 0, longitude: 0)
        let loc = locator.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if loc.range(of: "^[A-R]{2}[0-9]{2}$", options: .regularExpression) != nil {
            // print("\(loc) case 1")
        } else if loc.range(of: "^[A-R]{2}[0-9]{2}[A-X]{2}$", options: .regularExpression) != nil {
            let el1 = Double(loc.asciiArray[1]) - charA
            let el3 = Double(loc.asciiArray[3]) - char0
            let el5 = Double(loc.asciiArray[5]) - charA + 0.5
            let lat = el1 * 10 + el3 + el5 / 24 - 90
            
            let el0 = Double(loc.asciiArray[0]) - charA
            let el2 = Double(loc.asciiArray[2]) - char0
            let el4 = Double(loc.asciiArray[4]) - charA + 0.5
            let long = el0 * 20 + el2 * 2 + el4 / 12 - 180
            
            coord = CLLocation(latitude: lat, longitude: long)
        } else if loc.range(of: "^[A-R]{2}[0-9]{2}[A-X]{2}[0-9]{2}$", options: .regularExpression) != nil {
            print("\(loc) case 3")
        } else if loc.range(of: "^[A-R]{2}[0-9]{2}[A-X]{2}[0-9]{2}[A-X]{2}$", options: .regularExpression) != nil {
            print("\(loc) case 4")
        } else {
            print("\(loc) does't look as valid locator.")
        }
        
        return coord
    }
    
    /// Convert degrees to radians
    /// </summary>
    /// <param name="deg"></param>
    /// <returns></returns>
    func DegToRad(deg:Double) -> Double {
        return Double(deg / 180 * Double.pi)
    }
    
    func  stringToInt(inp:String) -> Float {
        let out:Float
        var input = inp.replacingOccurrences(of: " ", with: "")
        
        if (input.contains("S")) {
            input = input.replacingOccurrences(of: "S", with: "")
            out = NSString(string: input).floatValue * (-1)
        } else if (input.contains("W")) {
            input = input.replacingOccurrences(of: "W", with: "")
            out = NSString(string: input).floatValue * (-1)
        } else {
            input = input.replacingOccurrences(of: "N", with: "")
            input = input.replacingOccurrences(of: "E", with: "")
            out = NSString(string: input).floatValue
        }
        return out
    }
    
    func IEEEremainder(dividend:Double, divisor:Double) -> Double {
        return Double(dividend - (divisor * round(dividend / divisor)))
    }
}
