//
//  Aliases.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2017-06-23.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class Aliases: NSManagedObject{
    @NSManaged var alias: String!
    @NSManaged var dxcc_pref: String!
    @NSManaged var about:String!
}
