//
//  QSOTableViewHeader.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2018-02-20.
//  Copyright © 2018 Andrew Valender. All rights reserved.
//

import UIKit
import CoreData

class QsoTableViewHeader: UITableViewHeaderFooterView {
    // MARK: Properties
    
    var cellId: NSManagedObjectID?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(record: NSManagedObject) {
        cellId = record.objectID
        // configure the labels, etc in the cell
    }
    
}
