//
//  ADIFExportController.swift
//  QSO Secretary
//
//  Created by andrewv on 2017-08-16.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ADIFExportController: UIViewController {
    
    var clearDB:Bool = false
    
    @IBOutlet weak var startExportButton: UIButton!
    @IBAction func startExportClicked(_ sender: Any) {
        startExport()
    }
    
    @IBOutlet weak var deleteSwitch: UISwitch!
    @IBOutlet weak var exportProgress: UIProgressView!
    @IBOutlet weak var exportValue: UILabel!
    @IBOutlet weak var numExported: UILabel!
    
    @IBAction func cancel(_ sender: Any) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        
        /* https://www.dotnetperls.com/uibutton-swift
         and segues from https://medium.com/@mimicatcodes/create-unwind-segues-in-swift-3-8793f7d23c6f
         */
        let isPresentingInAddMealMode = presentingViewController is UINavigationController
        
        self.view.endEditing(true)
        
        if isPresentingInAddMealMode {
            dismiss(animated: true, completion: nil)
        }
        else {
            navigationController!.popViewController(animated: true)
        }
    }
    
    var maxNumOfRecords:Int = 0
    var counter:Int = 0 {
        didSet {
            let main = DispatchQueue.main
            main.async {
                let fractionalProgress = Float(self.counter) / Float(self.maxNumOfRecords)
                self.exportProgress.setProgress(fractionalProgress, animated: self.counter != 0)
                self.exportValue.text = ("\(Int(fractionalProgress * 100))% done.")
                print("\(fractionalProgress * 100)%")
                self.numExported.text = ("\(self.counter) QSO exported.")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exportProgress.setProgress(0, animated: true)
        exportProgress.isHidden = true
        startExportButton.isHidden = false
        
        deleteSwitch.addTarget(self, action: #selector(stateChanged), for: .valueChanged)
    }
    
    func startExport() -> Void{
        
        var records = [NSManagedObject]()
        let fetchedResultsController = CoreDataManager.instance.fetchedResultsController(entityName: "QSO", keyForSort: "date")
        
        do {
            try fetchedResultsController.performFetch()
            records = fetchedResultsController.fetchedObjects as! [NSManagedObject]
            self.maxNumOfRecords = records.count
        } catch let error as NSError {
            showERR(input: 2)
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        if self.maxNumOfRecords == 0 {
            showERR(input: -1)
        }
        
        exportProgress.isHidden = false
        startExportButton.isHidden = true
        self.counter = 0
        
        let queue = DispatchQueue.global(qos: .utility)
        queue.async{
            var text:String = ""
            let now = Date()
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            let dateString = formatter.string(from: now)
            
            // ADIF header
            text += "#\r\n# This data was exported using QSO Secretary for iPhone,"
            text += "\r\n# conforming to ADIF standart specification version 2.0\r\n"
            text += "# Created: "
            text += dateString
            text += "\r\n<EOH>\r\n"
            // Loop through and pack all records into one string
            
            for record in records {
                for index in 0..<QSO.adifFields.count
                {
                    if (QSO.adifFields[index][1].caseInsensitiveCompare("-") != .orderedSame) {
                        if (QSO.adifFields[index][2].caseInsensitiveCompare("QSO") == .orderedSame){
                            if (record.value(forKeyPath: QSO.adifFields[index][1]) != nil) {
                                var label = "<" + QSO.adifFields[index][0] + ":"
                                var value = record.value(forKeyPath: QSO.adifFields[index][1]) as! String
                                if value.count > 0 {
                                    if (QSO.adifFields[index][0].caseInsensitiveCompare("TIME_ON") == .orderedSame) {
                                        value = value.replacingOccurrences(of: ":", with: "")
                                    }
                                    if (QSO.adifFields[index][0].caseInsensitiveCompare("QSO_DATE") == .orderedSame) {
                                        value = value.replacingOccurrences(of: "-", with: "")
                                    }
                                    label += "\(value.count)>"
                                    text += label + value
                                    print(value)
                                }
                            }
                        } else if (QSO.adifFields[index][2].caseInsensitiveCompare("Designators") == .orderedSame){
                            if (record.value(forKeyPath: "designators.\(QSO.adifFields[index][1])") != nil) {
                                var label = "<" + QSO.adifFields[index][0] + ":"
                                let value = record.value(forKeyPath: "designators.\(QSO.adifFields[index][1])") as! String
                                if value.count > 0 {
                                    label += "\(value.count)>"
                                    text += label + value
                                    print(value)
                                }
                            }
                        } else if (QSO.adifFields[index][2].caseInsensitiveCompare("QSL_card") == .orderedSame){
                            if (record.value(forKeyPath: "cards.\(QSO.adifFields[index][1])") != nil) {
                                var label = "<" + QSO.adifFields[index][0] + ":"
                                let value = record.value(forKeyPath: "cards.\(QSO.adifFields[index][1])") as! String
                                if value.count > 0 {
                                    label += "\(value.count)>"
                                    text += label + value
                                    print(value)
                                }
                            }
                        }
                    }
                }
                text += "<EOR>\r\n"
                self.counter += 1
            }
            // Write string to file
            let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            // create a name for your image
            let fileName = "Exported_" + dateString.replacingOccurrences(of: ":", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " ", with: "") + ".adi"
            let fileURL = documentsDirectoryURL.appendingPathComponent(fileName)
            
            if !FileManager.default.fileExists(atPath: fileURL.path) {
                //writing
                do {
                    try text.write(to: fileURL, atomically: false, encoding: String.Encoding.windowsCP1251)
                }
                catch { }
                if self.clearDB {
                    print ("Deleting all records")
                    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
                    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
                    
                    do {
                        try CoreDataManager.instance.managedObjectContext.execute(deleteRequest)
                        CoreDataManager.instance.saveContext()
                    } catch {
                        print ("There was an error while deleting all records")
                    }
                }
                
            } else {
                self.showERR(input: 1)
            }
        }
        showERR(input: 0)
    }
    
    func setADIField(input:String) -> String{
        var result = "-"
        let labels = QSO.adifFields; //call the index array
        for label in labels {
            if(input.caseInsensitiveCompare(label[1]) == .orderedSame) {
                result = label[0]
                break
            }
        }
        return result
    }
    
    func showERR(input:Int) -> Void{
        switch (input) {
        case 0:
            
            break
            
        case -1:
            let main = DispatchQueue.main
            main.async {
                self.numExported.text = ("Nothing to export! Log is empty.")
                self.numExported.textColor = UIColor.red
            }
            break
            
        case 1:
            let main = DispatchQueue.main
            main.async {
                self.numExported.text = ("Exported file already exists!")
                self.numExported.textColor = UIColor.red
            }
            break
            
        case 2:
            let main = DispatchQueue.main
            main.async {
                self.numExported.text = ("Could not fetch data from Data base!")
                self.numExported.textColor = UIColor.red
            }
            break
            
            
        default:
            break
            
        }
    }
    
    @objc func stateChanged(switchState: UISwitch) {
        if switchState.isOn {
            clearDB = true
        } else {
            clearDB = false
        }
    }
}
