//
//  SatPickerViewController.swift
//  QSO Secretary
//
//  Created by Andrew Valender on 05/01/17.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import Foundation
import UIKit

protocol SatPickerViewControllerDelegate {
    func getEnteredNameValue(data: String)
}

class SatPickerViewController:  UIViewController, UIPickerViewDataSource, UIPickerViewDelegate  {
    var delegate: SatPickerViewControllerDelegate?
    
    var satNameValue:String!
    
    @IBOutlet weak var satNamePicker: UIPickerView!
    
    @IBAction func doneClicked(_ sender: AnyObject) {
        self.delegate?.getEnteredNameValue(data: satNameValue)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.satNamePicker.dataSource = self;
        self.satNamePicker.delegate = self;
        
        if let row = QSO.allSatelites.index(of: satNameValue) {
            satNamePicker.selectRow(row, inComponent: 0, animated: false)
        }
    }
    
    //MARK: - Delegates and data sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return QSO.allSatelites.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return QSO.allSatelites[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        satNameValue = QSO.allSatelites[row]
    }
}
