//
//  DateTimePickerViewController.swift
//  QSO Secretary
//
//  Created by Andrew Valender on 05/01/17.
//  Copyright © 2019 Andrew Valender. All rights reserved.
//

import Foundation
import UIKit

protocol DateTimePickerViewControllerDelegate {
    func getEnteredDateValue(data: Date)
}

class DateTimePickerViewController: UIViewController {
    var delegate: DateTimePickerViewControllerDelegate?
    
    var dateValue:String!
    var timeValue:String!
    
    @IBAction func doneClicked(_ sender: AnyObject) {
        self.delegate?.getEnteredDateValue(data: dateTimePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var dateTimePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let date = dateFormatter.date(from: (dateValue + " " + timeValue))
        
        dateTimePicker.setDate(date!, animated: false)
    }
}
