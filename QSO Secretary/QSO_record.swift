//
//  QSO.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 07/08/16.
//  Copyright © 2016 Natalia Mykhailovska. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class QSO_record {
    var band: String?
    var call: String?
    var continent: String?
    var date: String?
    var dbStatus: Bool = false
    var dxccCqITU: String?
    var eQSLStatus: String?
    var freq: String?
    var iotaRef: String?
    var locator: String?
    var mode: String?
    var name: String?
    var comment: String?
    var pref: String?
    var propagationMode: String?
    var qslStatus: Int16 = 0
    var qth: String?
    var rdaRef: String?
    var rstSent: String?
    var rstRvcd: String?
    var satMode: String?
    var satName: String?
    var sotaRef: String?
    var time: String?
    var wwffRef: String?
    var qsoId: NSManagedObjectID?
    var records:[NSManagedObject] = []
    var prefDBinfo: String?
    var prefDBcountry: String?
    var prefDBcontinent: String?
    var prefDBcq: String?
    var prefDBitu: String?
    var prefDBlokator: String?
    var prefDBstolica: String?
    var prefDBgmt: String?
    var prefDBhamatlas: String?
    var prefDBdxccEntity: String?
    var numberOfRecords:Int32 = 0
    //let managedContext: NSManagedObjectContext
    
    
    init() {
        self.band = "20m"
        self.call = "-"
    }
    
    func loadQSO(_ id:NSManagedObjectID) {
        
        let managedContext = CoreDataManager.instance.managedObjectContext
        do {
            let record = try managedContext.existingObject(with: id)
            
            self.time = record.value(forKeyPath: "time") as! String?
            self.date = record.value(forKeyPath: "date") as! String?
            self.call = record.value(forKeyPath: "call") as! String?
            self.band = record.value(forKeyPath: "band") as! String?
            self.freq = record.value(forKeyPath: "frequency") as! String?
            self.mode = record.value(forKeyPath: "mode") as! String?
            self.rstSent = record.value(forKeyPath: "rst_sent") as! String?
            self.rstRvcd = record.value(forKeyPath: "rst_rcvd") as! String?
            self.name = record.value(forKeyPath: "name") as! String?
            self.qth = record.value(forKeyPath: "qth") as! String?
            self.locator = record.value(forKeyPath: "locator") as! String?
            self.comment = record.value(forKeyPath: "comment") as! String?
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        //getCallbookRecord(call: self.call)
    }
    /*
    func recordHasDupes(_ input:QSO) -> Bool{
        var result = false
        let fetchedCalls:[NSManagedObject]
        let managedContext = CoreDataManager.instance.managedObjectContext
        let callsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
        
        let predicate1 = NSPredicate(format: "call == %@", input.call!)
        let predicate2 = NSPredicate(format: "date == %@", input.date!)
        let predicate3 = NSPredicate(format: "time == %@", input.time!)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1, predicate2, predicate3])
        
        callsFetch.predicate = predicateCompound
        do {
            fetchedCalls = try managedContext.fetch(callsFetch) as! [NSManagedObject]
            
            if fetchedCalls.count > 0 {
                result = true
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return result
    }
    
   func fetchRecords() {
        //1
        let managedContext = CoreDataManager.instance.managedObjectContext
        //2
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
        
        //3
        do {
            let results = try managedContext.fetch(fetchRequest)
            records = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func saveData(_ record:QSO) {
        //2
        let entity =  NSEntityDescription.entity(forEntityName: "QSO",
                                                 in:CoreDataManager.instance.managedObjectContext)!
        
        let logEntry = NSManagedObject(entity: entity,
                                       insertInto: CoreDataManager.instance.managedObjectContext)
        
        //3
        logEntry.setValue(record.time, forKey: "time")
        logEntry.setValue(record.date, forKey: "date")
        logEntry.setValue(record.call, forKey: "call")
        logEntry.setValue(record.band, forKey: "band")
        logEntry.setValue(record.frequency, forKey: "frequency")
        logEntry.setValue(record.mode, forKey: "mode")
        logEntry.setValue(record.rst_sent, forKey: "rst_sent")
        logEntry.setValue(record.rst_rcvd, forKey: "rst_rcvd")
        logEntry.setValue(record.name, forKey: "name")
        logEntry.setValue(record.qth, forKey: "qth")
        logEntry.setValue(record.locator, forKey: "locator")
        logEntry.setValue(record.comment, forKey: "comment")
        
        //4
        do {
            // Запись объекта
            CoreDataManager.instance.saveContext()
            //5
            records.append(logEntry)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        setDefaultValues()
    }
  
    // MARK: Delete one Data Record
    func remove(_ id:NSManagedObjectID) -> Void {
        
        let managedContext = CoreDataManager.instance.managedObjectContext
        
        do {
            let record = try managedContext.existingObject(with: id)
            
            managedContext.delete(record)
            
            do {
                try managedContext.save() // <- remember to put this
            } catch {
                print("Could not save \(error)")
            }
            
            
        } catch let error as NSError {
            print("Could not fetch \(error).")
        }
    }
    
    func setDefaultValues(){
        let defaults = UserDefaults.standard
        defaults.set(self.rst_sent, forKey: "lastRSTSentKey")
        defaults.set(self.rst_rcvd, forKey: "lastRSTRcvdKey")
        defaults.set(self.frequency, forKey: "lastFreqKey")
        defaults.set(self.band, forKey: "lastBandKey")
        defaults.set(self.mode, forKey: "lastModeKey")
        defaults.synchronize()
    }
    
    func removeAll() -> Void {
        let managedContext = CoreDataManager.instance.managedObjectContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error while deleting all records")
        }
    } */
}

