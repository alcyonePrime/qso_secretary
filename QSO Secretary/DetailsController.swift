//
//  DetailsController.swift
//  QSO Secretary
//
//  Created by mandr on 18/07/16.
//  Copyright © 2016 Andrew Valender. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class DetailsController: UIViewController, UITextFieldDelegate,UITextViewDelegate, BandPickerViewControllerDelegate, ModePickerViewControllerDelegate, DateTimePickerViewControllerDelegate, PropagationPickerViewControllerDelegate, SatPickerViewControllerDelegate,UIViewControllerTransitioningDelegate, UIPopoverPresentationControllerDelegate{
    
    var qso_record: QSO?
    let alias = Aliases()
    let currentDate = Date()
    var callsign = ""
    let dateFormatter = DateFormatter()
    static let DATEFORMAT = "yyyy-MM-dd"
    static let TIMEFORMAT = "HH:mm"
    
    
    @IBAction func cancel(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        if saveQso() {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var myInfo: UILabel!
    @IBOutlet weak var raportSent: UITextField!
    @IBOutlet weak var raportReceived: UITextField!
    @IBOutlet weak var hisName: UITextField!
    @IBOutlet weak var hisQTH: UITextField!
    @IBOutlet weak var hisLocator: UITextField!
    @IBOutlet weak var hisInfo: UITextView!
    @IBOutlet weak var callsignField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var hisBand: UILabel!
    @IBOutlet weak var hisMode: UILabel!
    @IBOutlet weak var hisDate: UILabel!
    @IBOutlet weak var hisFreq: UITextField!
    @IBOutlet weak var satName: UILabel!
    @IBOutlet weak var propMode: UILabel!
    @IBOutlet weak var l_cq: UILabel!
    @IBOutlet weak var l_itu: UILabel!
    @IBOutlet weak var extraInfoAndEQSL: UITextView!
    @IBOutlet weak var l_azimuth: UILabel!
    @IBOutlet weak var l_qrb: UILabel!
    
    @IBAction func send_eQSL(_ sender: Any) {
        // Check if user entered his passw and login
        let defaults = UserDefaults.standard
        var userid:String = ""
        if let stringOne = defaults.string(forKey: "settings_eQSLaccount") {
            userid = stringOne // Some String Value
        }
        var userpw:String = ""
        if let stringOne = defaults.string(forKey: "settings_eQSLpassw") {
            userpw = stringOne // Some String Value
        }
        
        var message:String = "eQSL will be sent right after you close this window."
        if (userid.isEmpty) || (userpw.isEmpty) {
            message = "Missing eQSL login or password. Please check settings."
        } else {
            sendEqsl = true
        }
        let noteController = UIAlertController(title: "Send eQSL", message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel) { action in
            // ...
        }
        noteController.addAction(closeAction)
        self.present(noteController, animated: true) {
            // ...
        }
    }
    
    var dateTime:String = ""
    var UTCtimeNow:String = ""
    var dateNow:String = ""
    var sendEqsl:Bool = false
    
    weak var activeField: UITextField?
    var viewMode:Int = 0  // 0 - log new QSO, 1 - edit old QSO from the log
    
    let eQSLresponse:[String] = [
        //Errors:
        /* 0 */    "File not saved",
                   /* 1 */    "Missing eQSL_User",
                              /* 2 */    "Missing eQSL_Pswd",
                                         /* 3 */    "No match on eQSL_User/eQSL_Pswd",
                                                    /* 4 */    "No match on eQSL_User/eQSL_Pswd for date",
                                                               /* 5 */    "Multiple accounts match eQSL_User/eQSL_Pswd for date",
                                                                          /* 6 */    "No match on eQSL_User/eQSL_Pswd for date",
                                                                                     /* 7 */    "Multiple accounts match eQSL_User/eQSL_Pswd for date",
                                                                                                
                                                                                                //  Warnings:
        /* 8 */    "Bad QSO Date:",
                   /* 9 */    "Bad QSO Time:",
                              /* 10 */    "Bad Callsign:",
                                          /* 11 */    "Bad Mode:",
                                                      /* 12 */    "Bad Band/Freq:",
                                                                  /* 13 */    "Bad Sat_Mode:",
                                                                              /* 14 */    "Bad record: Duplicate",
                                                                                          /* 15 */    "QSO Date/Time in Future:",
                                                                                                      /* 16 */    "Result: 1 out of"]
    
    let errMessages:[String] = [
        //Errors:
        /* 0 */    "File not saved",
                   /* 1 */    "Missing username, please check your eQSL settings and export QSO once again",
                              /* 2 */    "Missing password, please check your eQSL settings and export QSO once again",
                                         /* 3 */    "Unknown username, please check your eQSL settings and export QSO once again",
                                                    /* 4 */    "Unknown username, please check your eQSL settings and export QSO once again",
                                                               /* 5 */    "Multiple accounts match eQSL_User/eQSL_Pswd for date, please check your eQSL settings",
                                                                          /* 6 */    "Unknown eQSL user, please check your eQSL settings",
                                                                                     /* 7 */    "Multiple accounts match eQSL_User/eQSL_Pswd for date, please check your eQSL settings",
                                                                                                
                                                                                                //  Warnings:
        /* 8 */    "Bad QSO Date, please check and export once again",
                   /* 9 */    "Bad QSO Time, please check and export once again",
                              /* 10 */    "Bad Callsign, please check and export once again",
                                          /* 11 */    "Bad Mode, please check and export once again",
                                                      /* 12 */    "Bad Band/Freq, please check and export once again",
                                                                  /* 13 */    "Bad Sat_Mode, please check and export once again",
                                                                              /* 14 */    "Bad record: duplicated QSO, please check and export once again",
                                                                                          /* 15 */    "QSO Date/Time in Future, please check and export once again",
                                                                                                      /* 16 */    "QSO succesfully exported",
                                                                                                                  /* 17 */    "Unknown error, please try again later"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hisFreq.delegate = self
        raportSent.delegate = self
        raportReceived.delegate = self
        hisName.delegate = self
        hisQTH.delegate = self
        hisLocator.delegate = self
        hisInfo.delegate = self
        
        // Load existing contact
        if let qso_record = qso_record {
            UTCtimeNow = qso_record.time_on!
            if UTCtimeNow.count > 5 {
                UTCtimeNow = String(UTCtimeNow.prefix(5))
            }
            dateNow = qso_record.date!
            dateTime = dateNow + " " + UTCtimeNow
            callsignField.text = qso_record.call
            alias.getCallbookRecord(call: qso_record.call!)
            
            if ((qso_record.comment?.count)! > 1) {
                hisInfo.text = qso_record.comment
                hisInfo.textColor = UIColor.black
            } else {
                hisInfo.text = "Additional information about QSO"
                hisInfo.textColor = UIColor.lightGray
            }
            
            /* eQSL card send and received
            if (qso_record.eqsl_status ) {
            } */
            
        } else {
            // ... or Log new QSO
            qso_record = QSO()
            qso_record?.getDefaultValues()
            callsignField.text = callsign
            
            dateFormatter.dateFormat = DetailsController.DATEFORMAT
            dateNow = dateFormatter.string(from: currentDate as Date)
            
            dateFormatter.dateFormat = DetailsController.TIMEFORMAT
            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
            
            UTCtimeNow = dateFormatter.string(from: currentDate as Date)
            print("my time is \(UTCtimeNow) hours")
            dateTime = dateNow + " " + UTCtimeNow
            
            alias.getCallbookRecord(call: callsign)
            qso_record?.loadPreviousContacts(call: callsign)
            
            hisInfo.text = "Additional information about QSO"
            hisInfo.textColor = UIColor.lightGray
        }
        
        for i in 0..<QSO.bandsFreqs.count {
            if (QSO.bandsFreqs[i][0].caseInsensitiveCompare((qso_record?.band)!) == .orderedSame) {
                if (i < 11) { // index of 10 Meters band
                    satName.isHidden = true
                    propMode.isHidden = true
                } else {
                    satName.isHidden = false
                    propMode.isHidden = false
                }
                break
            }
        }
        
        let defaults = UserDefaults.standard
        let info = "My locator:"
        if let stringOne = defaults.string(forKey: "settings_myLocator") {
            var dbLocator:String = ""
            
            myInfo.text = info + " " + stringOne
            
            if let alk = alias.lokator {
                if alk.count > 1 {
                    dbLocator = alk
                    print(String(format: "Callbook locator \(alk)"))
                }
            }
            
            if let clk = qso_record?.designators?.gridsquare {
                if clk.count > 1 {
                    dbLocator = clk
                    print(String(format: "Log locator \(clk)"))
                }
            }
                
            if (dbLocator.count > 1) {
                let testCoord:CLLocation = CLLocation(latitude: 0, longitude: 0)
                
                let loc = GridLocator()
                let ll = loc.LocatorToLatLong(locator: dbLocator)
                let ml = loc.LocatorToLatLong(locator: stringOne)
                if testCoord != ll
                {
                // Calc distance
                let myLocation = CLLocation(latitude: ml.coordinate.latitude, longitude: ml.coordinate.longitude)
                let hisLocation = CLLocation(latitude: ll.coordinate.latitude, longitude: ll.coordinate.longitude)
                
                let distanceBetween: CLLocationDistance = myLocation.distance(from: hisLocation)
                print(String(format: "QRB is \(distanceBetween)"))
                let z = NSString(format: "%.0f", distanceBetween/1000)
                
                var targetString = "QRB: " + (z as String) + " km"
                l_qrb.attributedText = attributedString(from: targetString, nonBoldRange: NSMakeRange(0, 4))
                
                // Calc azimuth
                let azimuth = loc.Azimuth(LatA: ml.coordinate.latitude, LongA: ml.coordinate.longitude,
                                          LatB: ll.coordinate.latitude, LongB: ll.coordinate.longitude)
                let s = NSString(format: "%.0f", azimuth)
                
                targetString = "Az: " + (s as String) + "\u{00B0}"
                l_azimuth.attributedText = attributedString(from: targetString, nonBoldRange: NSMakeRange(0, 2))
                }
            }
        } else {
            myInfo.text = "Turn on GPS."
        }
        
        hisFreq.text = qso_record?.frequency
        raportSent.text = qso_record?.rst_sent
        raportReceived.text = qso_record?.rst_rcvd
        hisName.text = qso_record?.name
        hisQTH.text = qso_record?.qth
        hisLocator.text = qso_record?.designators?.gridsquare
        
        if let cq = alias.cq {
            if cq.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                let targetString = "CQ: " + cq
                l_cq.attributedText = attributedString(from: targetString, nonBoldRange: NSMakeRange(0, 2))
            }
        }
        
        if let itu = alias.itu {
            if itu.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                let targetString = "ITU: " + itu
                l_itu.attributedText = attributedString(from: targetString, nonBoldRange: NSMakeRange(0, 3))
            }
        }
        
        var stationTime:String = ""
        if var gmt = alias.gmt {
            let calendar = Calendar.current
            var hDiff = 0
            var mDiff = 0
            
            if gmt.contains(",") {
                let myStringArr = gmt.components(separatedBy: ",")
                gmt = myStringArr[0]
                let partHour = (myStringArr[0] as NSString).integerValue
                if (partHour == 5){
                    mDiff = 30
                } else if (partHour == 25){
                    mDiff = 15
                } else if (partHour == 75){
                    mDiff = 45
                } else {
                    mDiff = 0
                }
                print(String(format: "Minutes part  is \(mDiff)"))
            }
            
            hDiff = (gmt as NSString).integerValue
            // Check if daylight saving time is valid
            let tz = TimeZone.current
            if tz.isDaylightSavingTime(for: currentDate) {
                hDiff = hDiff + 1
            }
            
            let localT = calendar.date(byAdding: .minute, value: hDiff * 60 + mDiff, to: currentDate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            stationTime = dateFormatter.string(from: localT as! Date)
        }
        
        var boldText = 0
        var dxcc_str = ""
        if let dxcc_num = alias.dxcc_pref {
            if dxcc_num.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                dxcc_str = "DXCC: "  + dxcc_num + ". "
                boldText = 5
            }
        }
        var country_str = ""
        if let countr = alias.country {
            if countr.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                country_str = "Country: " + countr
                if let con = alias.continent {
                    country_str += ", " + con + ". "
                }
                if boldText == 0 {
                    boldText = 8
                }
            }
        }
        var about_str = ""
        if let inf = alias.prefDBinfo {
            if inf.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                about_str = inf + "."
            }
        }
        var targetString = ""
        if dxcc_str.count > 0 {
            targetString += dxcc_str
        }
        if country_str.count > 0 {
            targetString += country_str
            if about_str.count > 0 {
                targetString += "Info: " + about_str
            }
        } else {
            if about_str.count > 0 {
                targetString += "Info: " + about_str
            }
        }
        
        if (stationTime.count > 1) {
            targetString += " Local time: " + stationTime + "."
        }
        
        if (qso_record?.cards?.eqsl_qsl_sent == "Y" && qso_record?.cards?.eqsl_qsl_rcvd == "Y") {
            targetString += " eQSL sent & received."
        } else if (qso_record?.cards?.eqsl_qsl_sent == "Y") {
            targetString += " eQSL sent."
        } else if (qso_record?.cards?.eqsl_qsl_rcvd == "Y") {
            targetString += " eQSL received."
        }
        
        if (qso_record?.cards?.qsl_sent == "D") {
            targetString += " QSL card sent directly."
        } else if (qso_record?.cards?.qsl_sent == "B") {
            targetString += " QSL card sent via bureau."
        }
        
        extraInfoAndEQSL.text = targetString
        
        // get the date time String from the date object
        hisDate.text = dateTime
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(DetailsController.editDate))
        hisDate.addGestureRecognizer(tap1)
        
        hisBand.text = qso_record?.band
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(DetailsController.editBand))
        hisBand.addGestureRecognizer(tap2)
        
        hisMode.text = qso_record?.mode
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(DetailsController.editMode))
        hisMode.addGestureRecognizer(tap3)
        
        if let sat_name = qso_record?.sat_name {
            if sat_name.count < 3 {
                satName.text = QSO.allSatelites[0]
            } else {
                satName.text = sat_name
            }
        } else {
            satName.text = QSO.allSatelites[0]
        }
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(DetailsController.editSatName))
        satName.addGestureRecognizer(tap4)
      
        if let prop_name = qso_record?.prop_mode {
                propMode.text = getDescription(input: prop_name)
        } else {
            satName.text = QSO.allPropagation[0][0]
        }
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(DetailsController.editPropagation))
        propMode.addGestureRecognizer(tap5)
        
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        deregisterFromKeyboardNotifications()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.tag == 599)
        {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 3
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            
            print ("Keyboard size:", keyboardSize.height)
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            if (!aRect.contains(activeField.frame.origin)) {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //MARK:- UITextViewDelegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add additional information about QSO" {
            hisInfo.text = nil
            hisInfo.textColor = UIColor.black
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            hisInfo.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if hisInfo.text.isEmpty {
            hisInfo.text = "Add additional information about QSO"
            hisInfo.textColor = UIColor.lightGray
        }
    }
    
    func getEnteredDateValue(data: Date) {
        // Uses the data passed back
        dateFormatter.dateFormat = DetailsController.DATEFORMAT
        dateNow = dateFormatter.string(from: data)
        
        dateFormatter.dateFormat = DetailsController.TIMEFORMAT
        UTCtimeNow = dateFormatter.string(from: data)
        dateTime = dateNow + " " + UTCtimeNow
        hisDate.text = dateTime
    }
    
    func getEnteredBandValue(data: String) {
        // Uses the data passed back
        hisBand.text = data
        
    /*    for i in 0..<QSO.bandsFreqs.count {
            if (QSO.bandsFreqs[i][0].caseInsensitiveCompare(data) == .orderedSame) {
                if (i < 12) { // index of 10 Meters band
                    satName.isHidden = true
                    propMode.isHidden = true
                }
                break
            }
        } */
        
        for i in 0..<QSO.bandsFreqs.count {
            if (QSO.bandsFreqs[i][0].caseInsensitiveCompare(data) == .orderedSame) {
                hisFreq.text = QSO.bandsFreqs[i][1]
                if (i < 11) { // index of 10 Meters band
                    satName.isHidden = true
                    propMode.isHidden = true
                } else {
                    satName.isHidden = false
                    propMode.isHidden = false
                }
                break
            }
        }
    }
    
    func getEnteredModeValue(data: String) {
        // Uses the data passed back
        hisMode.text = data
        if (data == "CW") || (data == "PSK31")  || (data == "PSK63"){
            raportSent.text = "599"
            raportReceived.text = "599"
        } else {
            raportSent.text = "59"
            raportReceived.text = "59"
        }
    }
    
    func getEnteredNameValue(data: String) {
        // Uses the data passed back
        satName.text = data
    }
    
    func getEnteredPropagation(data: String) {
        // Uses the data passed back
        propMode.text = data
    }
    
    func saveQso() -> Bool{
        
        print ("details saved in DetailsController!")
        
        // Create object
        if qso_record == nil {
            qso_record = QSO()
        }
        
        // Saving object
        if let qso_record = qso_record {
            qso_record.date = dateNow
            qso_record.time_on = UTCtimeNow
            qso_record.rst_sent = self.raportSent.text!
            qso_record.rst_rcvd = self.raportReceived.text!
            qso_record.name = self.hisName.text
            qso_record.qth = self.hisQTH.text
            qso_record.designators?.gridsquare = self.hisLocator.text
            qso_record.comment = self.hisInfo.text
            qso_record.band = self.hisBand.text
            qso_record.mode = self.hisMode.text
            qso_record.call = self.callsignField.text
            qso_record.prop_mode = getAbbreviation(input: self.propMode.text ?? "")
            qso_record.sat_name = self.satName.text
            
            print("saved qso date and time: ", dateTime)
            
            CoreDataManager.instance.saveContext()
            
            if (sendEqsl) {
                //    createAndSend(eqsl: qso_record)
            }
        }
        self.view.endEditing(true)
        return true
    }
    
    @objc func editDate(sender:UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc : DateTimePickerViewController = storyboard.instantiateViewController(withIdentifier: "datePickerDialog") as! DateTimePickerViewController
        vc.delegate = self
        vc.dateValue = dateNow
        vc.timeValue = UTCtimeNow
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        vc.popoverPresentationController?.sourceView = self.view
        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        popover.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func editBand(sender:UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc : BandPickerViewController = storyboard.instantiateViewController(withIdentifier: "bandPickerDialog") as! BandPickerViewController
        vc.delegate = self
        vc.bandValue = hisBand.text!
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        vc.popoverPresentationController?.sourceView = self.view
        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        popover.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func editMode(sender:UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc : ModePickerViewController = storyboard.instantiateViewController(withIdentifier: "modePickerDialog") as! ModePickerViewController
        vc.delegate = self
        vc.modeValue = hisMode.text!
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        vc.popoverPresentationController?.sourceView = self.view
        
        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        popover.delegate = self
        self.present(vc, animated: true, completion:nil)
    }
    
    @objc func editSatName(sender:UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc : SatPickerViewController = storyboard.instantiateViewController(withIdentifier: "satPickerDialog") as! SatPickerViewController
        vc.delegate = self

        vc.satNameValue = satName.text!
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        vc.popoverPresentationController?.sourceView = self.view
        
        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        popover.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func editPropagation(sender:UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc : PropagationPickerViewController = storyboard.instantiateViewController(withIdentifier: "propagationPickerDialog") as! PropagationPickerViewController
        vc.delegate = self
        vc.propagationValue = propMode.text!
          print ("propagationValue :", propMode.text ?? QSO.allPropagation[0][0])
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        vc.popoverPresentationController?.sourceView = self.view
        
        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        popover.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        let navigationController = UINavigationController(rootViewController: controller.presentedViewController)
        let btnDone = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(DetailsController.dismiss as (DetailsController) -> () -> ()))
        navigationController.topViewController?.navigationItem.leftBarButtonItem = btnDone
        return navigationController
    }
    
    @objc func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createAndSend(eqsl:QSO) {
        let defaults = UserDefaults.standard
        
        var userid:String = ""
        if let stringOne = defaults.string(forKey: "settings_eQSLaccount") {
            userid = stringOne // Some String Value
        }
        
        var userpw:String = ""
        if let stringOne = defaults.string(forKey: "settings_eQSLpassw") {
            userpw = stringOne // Some String Value
        }
        
        // Spaces ruin server parce function.
        var eqslComments:String = ""
        if let stringOne = defaults.string(forKey: "settings_eQSLcomments") {
            eqslComments = stringOne.replacingOccurrences(of: " ", with: "%20")
        }
        
        /* generate the string to call default web browser */
        let abs_addr = "https://www.eqsl.cc/qslcard/ImportADIF.cfm"
        let s1 = "ADIFData=upload%20%3cadIF%5Fver%3A5%3e2.1.9"
        let s2 = "%3cEQSL%5FUSER%3A\(userid.count)"
        let s20 = "%3e\(userid)%3cEQSL%5FPSWD%3A"
        let s21 = "\(userpw.count)%3e\(userpw)%3cPROGRAMID%3A6%3eQSO%20secretary%3cEOH%3e"
        let s3 = "%3cBAND%3A\(eqsl.band!.count)%3e\(eqsl.band!.uppercased())"
        let s4 = "%3cCALL%3A\(eqsl.call!.count)%3e\(eqsl.call!.uppercased())"
        let s5 = "%3cMODE%3A\(eqsl.mode!.count)%3e\(eqsl.mode!.uppercased())"
        let s6 = "%3cQSO_DATE%3A\(eqsl.date!.count)%3e\(eqsl.date!)"
        let s7 = "%3cTIME_ON%3A\(eqsl.time_on!.count)%3e\(eqsl.time_on!)"
        let s8 = "%3cRST_SENT%3A\(eqsl.rst_sent!.count)%3e\(eqsl.rst_sent!)"
        let sn = String(eqslComments.filter { !"\n\t\r".contains($0) })
        let s9 = "%3cQSLMSG%3A\(sn.count)%3e\(sn)"
        
        var request = URLRequest(url: URL(string: abs_addr)!)
        request.httpMethod = "POST"
        let postString = s1 + s2 + s20 + s21 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + "%3cEOR%3e"
        request.httpBody = postString.data(using: .ascii)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                //print("error=\(error)")
                self.showeQSLresult(response: self.chkResponse(serverResponse: error as! String))
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                //print("response = \(response)")
                self.showeQSLresult(response: self.chkResponse(serverResponse: response as! String))
            }
            
            let responseString = String(data: data, encoding: .utf8)
            //print("responseString = \(responseString)")
            self.showeQSLresult(response: self.chkResponse(serverResponse: responseString!))
        }
        
        print("HTML request = \(postString)")
        task.resume()
    }
    
    func showeQSLresult(response:String) {
        let alertController = UIAlertController(title: "eQSL has been sent!", message: response, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "Close", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func chkResponse(serverResponse:String) -> String{
        var i:Int = 0
        for resp in eQSLresponse
        {
            if (resp.contains(serverResponse)) {
                break
            }
            i = i+1
        }
        return errMessages[i]
    }
    
    func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
        let fontSize = UIFont.systemFontSize
        let attrs = [
            convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.boldSystemFont(ofSize: fontSize),
            convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.black
        ]
        let nonBoldAttribute = [
            convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.systemFont(ofSize: fontSize),
            ]
        let attrStr = NSMutableAttributedString(string: string, attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs))
        if let range = nonBoldRange {
            attrStr.setAttributes(convertToOptionalNSAttributedStringKeyDictionary(nonBoldAttribute), range: range)
        }
        return attrStr
    }
        
        func getDescription(input:String) -> String{
            if (!input.isEmpty) {
                let labels = QSO.allPropagation;
                for label in labels {
                    if(input.caseInsensitiveCompare(label[1]) == .orderedSame) {
                        return label[0]
                    }
                }
            }
            return QSO.allPropagation[0][0]
        }
    func getAbbreviation(input:String) -> String{
        if (!input.isEmpty) {
            let labels = QSO.allPropagation;
            for label in labels {
                if(input.caseInsensitiveCompare(label[0]) == .orderedSame) {
                    return label[1]
                }
            }
        }
        return ""
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let input = input else { return nil }
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
