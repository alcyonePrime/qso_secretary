//
//  Callbook_aliases.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2017-06-19.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class Callbook: NSManagedObject{
    @NSManaged var dxcc_pref: String!
    @NSManaged var parent: String!
    @NSManaged var country:String!
    @NSManaged var continent: String!
    @NSManaged var cq: String!
    @NSManaged var itu:String!
    @NSManaged var lokator: String!
    @NSManaged var stolica: String!
    @NSManaged var gmt:String!
    @NSManaged var hamatlas:String!
}
