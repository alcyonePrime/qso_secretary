//
//  MaxLengthTextField.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2018-01-18.
//  Copyright © 2018 Andrew Valender. All rights reserved.
//

import UIKit

// 1
class MaxLengthTextField: UITextField, UITextFieldDelegate {
    
    // 2
    private var characterLimit: Int?
    
    // 3
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        delegate = self
    }
    
    // 4
    @IBInspectable var maxLength: Int {
        get {
            guard let length = characterLimit else {
                return Int.max
            }
            return length
        }
        set {
            characterLimit = newValue
        }
    }
    
    // 5
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // 6
        guard string.count > 0 else {
            return true
        }
        
        // 7
        let currentText = textField.text ?? ""
        // 8
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        // 9
        return allowedIntoTextField(text: prospectiveText)
    }
    
    func allowedIntoTextField(text: String) -> Bool {
        return text.count <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
}
