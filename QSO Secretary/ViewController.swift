//
//  ViewController.swift
//  QSO Secretary
//
//  Created by mandr on 18/07/16.
//  Copyright © 2016 Andrew Valender. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import CoreMotion

class ViewController: UIViewController, NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    let searchController = UISearchController(searchResultsController: nil)
    private let locationManager: CLLocationManager = CLLocationManager()
    private let motionManager = CMMotionActivityManager()
    var startLocation: CLLocation!
    var shouldShowSearchResults = false
    var fetchedResultsController = CoreDataManager.instance.fetchedResultsController(entityName: "QSO", keyForSort: "date")
    
    @IBAction func optionsSelected(_ sender: AnyObject) {
        // https://www.infragistics.com/community/blogs/stevez/archive/2013/03/04/associate-a-file-type-with-your-ios-application.aspx 
        let alertController = UIAlertController(title: nil, message: "Import ADIF: pick up needed ADI file in File Explorer, select \"Open as...\" and choose QSO Secretary", preferredStyle: .actionSheet)
        
        let exportAction = UIAlertAction(title: "Export log", style: .default) { action in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myVC = storyboard.instantiateViewController(withIdentifier: "ADIFExport") as! ADIFExportController
            self.navigationController!.pushViewController(myVC, animated: true)
        }
        alertController.addAction(exportAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // ...
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true) {
            // ...
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        
        //  Uncomment when need to track Settings changes (E.g  Night mode enabled)
        registerSettingsBundle()
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.GPSsettingsChanged), name: UserDefaults.didChangeNotification, object: nil)
        GPSsettingsChanged()
        
        // Do any additional setup after loading the view, typically from a nib.
        configureSearchController()
        
        configureGPS()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // http://apprize.info/apple/iphone_3/12.html
        /* let app = UIApplication.shared
         NotificationCenter.default.addObserver(self, selector: #selector(UIApplicationDelegate.applicationWillEnterForeground(_:)),
         name: NSNotification.Name.UIApplicationWillEnterForeground,
         object: app) */
        
        // Load empty state view if necessary.
        if tableView.numberOfRows(inSection: 0) == 0 {
            let emptyStateLabel = UILabel(frame: tableView.frame)
            emptyStateLabel.text = "No records. Import ADI file or type a callsign above."
            
            // style it as necessary
            emptyStateLabel.numberOfLines = 2
            emptyStateLabel.textAlignment = .center
            
            tableView.backgroundView = emptyStateLabel
        } else {
            tableView.backgroundView = nil
        }
        // Refresh table, especially after returning from export screen
        noFilterContent()
    }
    
    func applicationWillEnterForeground(notification:NSNotification) {
        
        let defaults = UserDefaults.standard
        defaults.synchronize()
    }
    
    
    func configureGPS()
    {
        let isUsed:Bool = UserDefaults.standard.bool(forKey: "settings_useGPS" )
        if isUsed {
            // https://habr.com/post/275769/
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.pausesLocationUpdatesAutomatically = false
            setActiveMode(true)
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            startLocation = nil
            motionManager.startActivityUpdates(to: .main, withHandler: { [weak self] activity in
                self?.setActiveMode(activity?.cycling ?? false)
            })
        }
    }
    
    func disableGPS()
    {
            setActiveMode(false)
            locationManager.stopUpdatingLocation()
            locationManager.stopMonitoringSignificantLocationChanges()
            startLocation = nil
            motionManager.stopActivityUpdates()
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation])
    {
        let latestLocation: CLLocation = locations[locations.count - 1]
        
        if startLocation == nil {
            startLocation = latestLocation
        }
        /* let distanceBetween: CLLocationDistance =
         latestLocation.distance(from: startLocation)
         print(String(format: "%.2f km", distanceBetween))  */
        
        let loc = GridLocator()
        let pringLocator = loc.LatLongToLocator(Lat: latestLocation.coordinate.latitude, Long: latestLocation.coordinate.longitude)
        if let currentLocator:String = UserDefaults.standard.string(forKey: "settings_myLocator")
        {
            if pringLocator != currentLocator {
                let message:String = "Would you like to use it?"
                let noteController = UIAlertController(title: "New locator detected!", message: message, preferredStyle: .alert)
                
                let yesAction = UIAlertAction(title: "Yes", style: .default) { action in
                    UserDefaults.standard.set(pringLocator, forKey: "settings_myLocator")
                }
                noteController.addAction(yesAction)
                
                let closeAction = UIAlertAction(title: "No, use old locator.", style: .cancel) { action in
                    // ...
                }
                noteController.addAction(closeAction)
                self.present(noteController, animated: true) {
                    // ...
                }
            }
        } else {
            UserDefaults.standard.set(pringLocator, forKey: "settings_myLocator")
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
    }
    
    private func setActiveMode(_ value: Bool) {
        if value {
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.distanceFilter = 10
        } else {
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.distanceFilter = CLLocationDistanceMax
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty == true {
            tableView.backgroundView = nil
            // noFilterContent()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        shouldShowSearchResults = true
        tableView.reloadData()
        searchController.searchBar.resignFirstResponder()
        
        if let _ = searchBar.text {
            // now you can call 'performSegue'
            performSegue(withIdentifier: "logToDetails", sender: nil)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text?.isEmpty != true {
            shouldShowSearchResults = true
            tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tableView.backgroundView = nil
        noFilterContent()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        // Always show the search result controller
        searchController.searchResultsController?.view.isHidden = false
        
        // Update your search results data and reload data
        filterContentForSearchText(searchString: searchController.searchBar.text!)
    }
    
    // MARK: - TableView Methods
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        //header.textLabel?.font = UIFont(name: "Futura", size: 11)
        header.textLabel?.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView.sectionHeaderHeight
    }
    
    // Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // use as? or as! to cast UITableViewCell to your desired type
        if(indexPath.row % 2 == 0) {
            cell.backgroundColor = UIColor.customYellow1
        } else {
            cell.backgroundColor = UIColor.customYellow2
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let managedObject = fetchedResultsController.object(at: indexPath) as! NSManagedObject
            CoreDataManager.instance.managedObjectContext.delete(managedObject)
            CoreDataManager.instance.saveContext()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let qso_record = fetchedResultsController.object(at: indexPath) as? QSO
        
        // performSegue(withIdentifier: "logToDetails", sender: qso_record)
        
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "logToDetails", sender: qso_record)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logToDetails" {
            if sender == nil {
                let detail:DetailsController = segue.destination as! DetailsController
                detail.callsign = searchController.searchBar.text!.uppercased()
                tableView.backgroundView = nil
                noFilterContent()
                shouldShowSearchResults = false
                searchController.searchBar.text = ""
            }
            let controller = segue.destination as! DetailsController
            controller.qso_record = sender as? QSO
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sectionInfo = fetchedResultsController.sections?[section] else { fatalError("Unexpected Section") }
        return sectionInfo.name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: QsoTableViewCell.reuseIdentifier, for: indexPath) as? QsoTableViewCell else {
            fatalError("Unexpected Index Path")
        }
        
        if self.validateIndexPath(indexPath) {
            allRecords( cell, at: indexPath)
        } else {
            print("Attempting to configure a cell for an indexPath that is out of bounds: \(indexPath)")
        }
        
        return cell
    }
    
    func validateIndexPath(_ indexPath: IndexPath) -> Bool {
        if let sections = self.fetchedResultsController.sections,
            indexPath.section < sections.count {
            if indexPath.row < sections[indexPath.section].numberOfObjects {
                return true
            }
        }
        return false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections.count
    }
    
    func allRecords(_ cell: QsoTableViewCell, at indexPath: IndexPath) {
        // Fetch record
        let record = fetchedResultsController.object(at: indexPath) as! QSO
        
        cell.cellId = record.objectID
        
        let totalRowNumber = fetchedResultsController.fetchedObjects?.count
        
        // Get row number from viewTable with sections
        var rowNumber = indexPath.row
        for i in 0..<indexPath.section {
            rowNumber += tableView.numberOfRows(inSection: i)
        }
        
        // Configure Cell
        cell.numberLabel?.text = "\(totalRowNumber! - rowNumber)"
        
        if let time = record.time_on {
            let pos = time.index(time.startIndex, offsetBy: 4)
            cell.timeLabel?.text = String(time[...pos])     // pos is an index, it works
        }
        cell.callsignLabel?.text = record.call
        cell.bandLabel?.text = record.band
        cell.modeLabel?.text = record.mode
    }
    
    func configureSearchController() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.autoresizingMask = .flexibleRightMargin
        searchController.searchBar.sizeToFit()
        searchController.searchBar.autocapitalizationType = UITextAutocapitalizationType.allCharacters
        searchController.searchBar.placeholder = "Type a call sign here.."
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        //self.tableView.tableHeaderView?.addSubview(searchController.searchBar)
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        searchController.searchBar.showsCancelButton = false
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            guard let indexPath = newIndexPath else { return }
            tableView.insertRows(at: [indexPath], with: .fade)
            break;
            
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .fade)
            break;
            
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) as? QsoTableViewCell {
                allRecords(cell, at: indexPath)
            }
            break;
            
        case .move:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            guard let newIndexPath = newIndexPath else { return }
            tableView.insertRows(at: [newIndexPath], with: .fade)
            break;
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            break;
        }
    }
    
    func filterContentForSearchText(searchString: String) {
        // https://www.appcoda.com/custom-search-bar-tutorial/
        if searchString.isEmpty == true {
            return
        }
        
        fetchedResultsController.fetchRequest.predicate = NSPredicate(format: "call CONTAINS %@", searchString.uppercased())
        
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    func noFilterContent() {
        
        fetchedResultsController.fetchRequest.predicate = nil
        
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    func registerSettingsBundle(){
        let appDefaults = [String:AnyObject]()
        UserDefaults.standard.register(defaults: appDefaults)
    }
    
    @objc func GPSsettingsChanged(){
        if UserDefaults.standard.bool(forKey: "settings_useGPS") {
            configureGPS()
        }
        else {
            disableGPS()
        }
    }
}
