    //
    //  AppDelegate.swift
    //  QSO Secretary
    //
    //  Created by Natalia Mykhailovska on 18/07/16.
    //  Copyright © 2016 Natalia Mykhailovska. All rights reserved.
    //
    
    import UIKit
    import Fabric
    import Crashlytics
    import Foundation
    import CoreData
    
    @UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate {
        
        var window: UIWindow?
        
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool{
            
            Fabric.with([Crashlytics.self])
            
            let defaults = UserDefaults.standard
            let isPreloaded = defaults.bool(forKey: "isPreloaded")
            if !isPreloaded {
                preloadData()
                defaults.set(true, forKey: "isPreloaded")
            }
            
            // Override point for customization after application launch.
            return true
        }
        
        func application(_ application: UIApplication, open Url: URL, sourceApplication: String?, annotation: Any) -> Bool {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myVC = storyboard.instantiateViewController(withIdentifier: "ADIFImport") as! ADIFImportController
            myVC.linkPassed = Url
            
            let navigationController = self.window?.rootViewController as! UINavigationController
            navigationController.pushViewController(myVC, animated: true)
            
            return true
        }
        
        func applicationWillResignActive(_ application: UIApplication) {
            // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
            // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        }
        
        func applicationDidEnterBackground(_ application: UIApplication) {
            // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
            // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
            // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        }
        
        func applicationDidBecomeActive(_ application: UIApplication) {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        }
        
        func applicationWillTerminate(_ application: UIApplication) {
            // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        }
        
        func parseAliases (contentsOfURL: NSURL, encoding: String.Encoding, error: NSErrorPointer) -> [(alias:String, dxcc_pref:String, about: String)]? {
            // Load the CSV file and parse it
            let delimiter = ";"
            var items:[(alias:String, dxcc_pref:String, about: String)]?
            var content = ""
            
            do {
                content = try String(contentsOf: contentsOfURL as URL, encoding: encoding)
            }
            catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror.localizedDescription)")
            }
            
            if  !content.isEmpty {
                items = []
                let lines:[String] = content.components(separatedBy:NSCharacterSet.newlines) as [String]
                
                for line in lines {
                    var values:[String] = []
                    if line != "" {
                        // For a line with double quotes
                        // we use NSScanner to perform the parsing
                        if line.range(of:"\"") != nil {
                            var textToScan:String = line
                            var value:NSString?
                            var textScanner:Scanner = Scanner(string: textToScan)
                            while textScanner.string != "" {
                                
                                if (textScanner.string as NSString).substring(to: 1) == "\"" {
                                    textScanner.scanLocation += 1
                                    textScanner.scanUpTo("\"", into: &value)
                                    textScanner.scanLocation += 1
                                } else {
                                    textScanner.scanUpTo(delimiter, into: &value)
                                }
                                // Store the value into the values array
                                values.append(value! as String)
                                
                                // Retrieve the unscanned remainder of the string
                                if textScanner.scanLocation < textScanner.string.count {
                                    textToScan = (textScanner.string as NSString).substring(from: textScanner.scanLocation + 1)
                                } else {
                                    textToScan = ""
                                }
                                textScanner = Scanner(string: textToScan)
                            }
                            
                            // For a line without double quotes, we can simply separate the string
                            // by using the delimiter (e.g. comma)
                        } else  {
                            values = line.components(separatedBy:delimiter)
                        }
                        
                        // Put the values into the tuple and add it to the items array
                        let item = (alias: values[0], dxcc_pref: values[1], about: values[2])
                        items?.append(item)
                    }
                }
            }
            return items
        }
        
        func parseCallbook (contentsOfURL: NSURL, encoding: String.Encoding, error: NSErrorPointer) -> [(dxcc_pref:String, parent:String, country: String, continent:String, cq:String, itu:String, lokator:String, stolica:String, gmt:String, hamatlas:String, dxcc_entity:String)]? {
            // Load the CSV file and parse it
            let delimiter = ";"
            var items:[(dxcc_pref:String, parent:String, country:String, continent:String, cq:String, itu:String, lokator:String, stolica:String, gmt: String, hamatlas:String, dxcc_entity:String)]?
            var content = ""
            
            do {
                content = try String(contentsOf: contentsOfURL as URL, encoding: encoding)
            }
            catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror.localizedDescription)")
            }
            
            if  !content.isEmpty {
                items = []
                let lines:[String] = content.components(separatedBy:NSCharacterSet.newlines) as [String]
                
                for line in lines {
                    var values:[String] = []
                    if line != "" {
                        // For a line with double quotes
                        // we use NSScanner to perform the parsing
                        if line.range(of:"\"") != nil {
                            var textToScan:String = line
                            var value:NSString?
                            var textScanner:Scanner = Scanner(string: textToScan)
                            while textScanner.string != "" {
                                
                                if (textScanner.string as NSString).substring(to: 1) == "\"" {
                                    textScanner.scanLocation += 1
                                    textScanner.scanUpTo("\"", into: &value)
                                    textScanner.scanLocation += 1
                                } else {
                                    textScanner.scanUpTo(delimiter, into: &value)
                                }
                                // Store the value into the values array
                                values.append(value! as String)
                                
                                // Retrieve the unscanned remainder of the string
                                if textScanner.scanLocation < textScanner.string.count {
                                    textToScan = (textScanner.string as NSString).substring(from: textScanner.scanLocation + 1)
                                } else {
                                    textToScan = ""
                                }
                                textScanner = Scanner(string: textToScan)
                            }
                            
                            // For a line without double quotes, we can simply separate the string
                            // by using the delimiter (e.g. comma)
                        } else  {
                            values = line.components(separatedBy:delimiter)
                        }
                        
                        // Put the values into the tuple and add it to the items array
                        let item = (dxcc_pref:values[0], parent:values[1], country:values[2], continent:values[3], cq:values[4], itu:values[5], lokator:values[6], stolica:values[7], gmt:values[8], hamatlas:values[9], dxcc_entity:values[10])
                        items?.append(item)
                    }
                }
            }
            return items
        }
        
        
        func preloadData () {
            
            // Remove all the menu items before preloading
            //removeData()
            
            var error:NSError?
            // https://drive.google.com/uc?export=download&id=FILE_ID
            var remoteURL = NSURL(string: "https://drive.google.com/uc?export=download&id=0BwEdW5XeucKbY1Z5MWhVcVYwZzQ")!
            if let items = parseAliases(contentsOfURL: remoteURL, encoding: String.Encoding.windowsCP1250, error: &error) {
                // Preload items
                let managedContext = CoreDataManager.instance.managedObjectContext
                
                for item in items {
                    let entity =  NSEntityDescription.entity(forEntityName: "Aliases", in:managedContext)
                    let aliasItem = NSManagedObject(entity: entity!, insertInto: managedContext)
                    
                    aliasItem.setValue(item.alias, forKey: "alias")
                    aliasItem.setValue(item.dxcc_pref, forKey: "dxcc_pref")
                    aliasItem.setValue(item.about, forKey: "about")
                    
                    do {
                        try managedContext.save()
                    } catch {
                        // Replace this implementation with code to handle the error appropriately.
                        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                        let nserror = error as NSError
                        NSLog("Unresolved error \(nserror.localizedDescription)")
                        abort()
                    }
                }
            }
            // https://drive.google.com/open?id=0BwEdW5XeucKbZ2p4dEtiekN4ZFE
            // DXCCPrefix_Parent_Country_Continent_CQ_ITU_Lokator_Stolica_GMT_hamatlas
            remoteURL = NSURL(string: "https://drive.google.com/uc?export=download&id=0BwEdW5XeucKbZ2p4dEtiekN4ZFE")!
            if let items = parseCallbook(contentsOfURL: remoteURL, encoding: String.Encoding.windowsCP1250, error: &error) {
                // Preload items
                let managedContext = CoreDataManager.instance.managedObjectContext
                for item in items {
                    
                    let entity =  NSEntityDescription.entity(forEntityName: "Callbook", in:managedContext)
                    let aliasItem = NSManagedObject(entity: entity!, insertInto: managedContext)
                    
                    aliasItem.setValue(item.dxcc_pref, forKey: "dxcc_pref")
                    aliasItem.setValue(item.parent, forKey: "parent")
                    aliasItem.setValue(item.country, forKey: "country")
                    aliasItem.setValue(item.continent, forKey: "continent")
                    aliasItem.setValue(item.cq, forKey: "cq")
                    aliasItem.setValue(item.itu, forKey: "itu")
                    aliasItem.setValue(item.lokator, forKey: "lokator")
                    aliasItem.setValue(item.stolica, forKey: "stolica")
                    aliasItem.setValue(item.gmt, forKey: "gmt")
                    aliasItem.setValue(item.hamatlas, forKey: "hamatlas")
                    aliasItem.setValue(item.dxcc_entity, forKey: "dxcc_entity")
                    do {
                        try managedContext.save()
                    } catch {
                        // Replace this implementation with code to handle the error appropriately.
                        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                        let nserror = error as NSError
                        NSLog("Unresolved error \(nserror.localizedDescription)")
                        abort()
                    }
                }
            }
        }
    }
