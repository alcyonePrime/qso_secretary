//
//  ADIFImportController.swift
//  QSO Secretary
//
//  Created by Andrew Valender on 2017-08-15.
//  Copyright © 2017 Andrew Valender. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ADIFImportController: UIViewController {
    
    @IBOutlet weak var percentageImported: UILabel!
    @IBOutlet weak var importProgress: UIProgressView!
    @IBOutlet weak var totalImported: UILabel!
    @IBOutlet weak var duplicateImported: UILabel!
    @IBOutlet var importView: UIView!
    
    var maxNumOfRecords:Int32 = 0
    var qsoTotal:Int32 = 0 {
        didSet{
            let main = DispatchQueue.main
            main.async {
                self.importProgress.isHidden = false
                let fractionalProgress = Float(self.qsoTotal) / Float(self.maxNumOfRecords)
                
                self.importProgress.setProgress(fractionalProgress, animated: self.numImportedQSO != 0)
                self.percentageImported.text = ("\(Int(fractionalProgress * 100))% done.")
                
                self.totalImported.text = ("\(self.numImportedQSO) QSO imported.")
                
                if (self.numDuplicate != 0){
                    self.duplicateImported.text = ("\(self.numDuplicate) duplicate QSO")
                    self.duplicateImported.isHidden = true
                }
            }
        }
    }
    
    var linkPassed:URL!
    var qso_record: QSO?
    var errorInRecord = false
    //var callCompare: String = ""
    var dateCompare: String = ""
    var timeCompare: String = ""
    var numImportedQSO:Int32 = 0
    var numDuplicate:Int32 = 0
    var eQSLDownload: Bool = false
    let labels = QSO.adifFields; //call the index array
    
    struct Import {
        var call: String = ""
        var date: String = ""
        var time: String = ""
        var freq: String = ""
        var band: String = ""
        var mode: String = ""
        var submode: String = ""
        var rst_in: String = ""
        var rst_out: String = ""
        var name: String = ""
        var qth: String = ""
        var locator: String = ""
        var continent: String = ""
        var comment: String = ""
        var sat_name: String = ""
        var propagation_mode: String = ""
        var iota_ref: String = ""
        var sota_ref: String = ""
        var eqsl_sent: String = ""
        var eqsl_rcvd: String = ""
        var message: String = ""
        var qsl_sent: Bool = false
        var qsl_rcvd: Bool = false
        var qsl_rcvd_via: String = ""
        var qsl_sent_via: String = ""
        var lotw_qsl_sent: String = ""
        var lotw_qsl_rcvd: String = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        importProgress.setProgress(0, animated: true)
        importProgress.isHidden = true
        self.duplicateImported.isHidden = true
        
        let queue = DispatchQueue.global(qos: .utility)
        queue.async{
            self.readAndParse()
        }
    }
    
    func readAndParse() {
        var lineBuffer:String = ""
        var fileSize:Int = 0
        var inputArray = [String]()
        
        do {
            let data = try String(contentsOf: self.linkPassed, encoding: String.Encoding.windowsCP1251)
            fileSize = data.count
            inputArray = data.components(separatedBy: "<EOR>")
            if (inputArray.count <= 1) {
                inputArray = data.components(separatedBy: "<eor>")
            }
        } catch {
            print(error)
            let main = DispatchQueue.main
            main.async {
                self.totalImported.text = ("Unable to import! File is empty or damaged.")
                return
            }
        }
        
        // Calculate total number of records
        self.maxNumOfRecords = Int32(inputArray.count) - 1
        print ("fileSize: \(fileSize) , total number of records: \(self.maxNumOfRecords)")
        
        for myStr in inputArray {
            lineBuffer += ", " + myStr
            
            var buf = lineBuffer.replacingOccurrences(of: "\n", with: "")
            buf = buf.replacingOccurrences(of: "\r", with: "")
            let validation = self.parseBuffer(lineBuffer: buf)
            if (validation.1 == 0b11111) {
                
                if !someEntityExists(call: validation.0.call, date: validation.0.date, time: validation.0.time){
                    // Create object
                    qso_record = QSO()
                    qso_record?.call = validation.0.call
                    print(validation.0.call)
                    qso_record?.date = validation.0.date
                    qso_record?.time_on = validation.0.time
                    qso_record?.frequency = validation.0.freq
                    qso_record?.band = validation.0.band
                    if (validation.0.submode == "") {
                        qso_record?.mode = validation.0.mode
                    } else {
                        qso_record?.mode = validation.0.submode
                    }
                    qso_record?.rst_rcvd = validation.0.rst_in
                    qso_record?.rst_sent = validation.0.rst_out
                    qso_record?.name = validation.0.name
                    qso_record?.qth = validation.0.qth
                    qso_record?.designators?.gridsquare = validation.0.locator
                    qso_record?.designators?.iota_ref = validation.0.iota_ref
                    qso_record?.designators?.sota_ref = validation.0.sota_ref
                    qso_record?.comment = validation.0.comment
                    qso_record?.sat_name = validation.0.sat_name
                    qso_record?.prop_mode = validation.0.propagation_mode
                    qso_record?.cards?.qslmsg = validation.0.message
                    qso_record?.cards?.eqsl_qsl_sent = validation.0.eqsl_sent
                    if (eQSLDownload) {
                        qso_record?.cards?.eqsl_qsl_rcvd = "Y"
                    } else {
                        qso_record?.cards?.eqsl_qsl_rcvd = validation.0.eqsl_rcvd
                    }
                    
                    qso_record?.cards?.lotw_qsl_sent = validation.0.lotw_qsl_sent
                    qso_record?.cards?.lotw_qsl_rcvd = validation.0.lotw_qsl_rcvd

                    
                    if (validation.0.qsl_sent && validation.0.qsl_sent_via.caseInsensitiveCompare("E") == .orderedSame) {
                        qso_record?.cards?.eqsl_qsl_sent = "Y"
                    } else if (validation.0.qsl_sent && validation.0.qsl_sent_via.caseInsensitiveCompare("B") == .orderedSame) {
                        qso_record?.cards?.qsl_sent = "B"
                    } else if (validation.0.qsl_sent && validation.0.qsl_sent_via.caseInsensitiveCompare("D") == .orderedSame) {
                        qso_record?.cards?.qsl_sent = "D"
                    }
                    
                    if (validation.0.qsl_rcvd && validation.0.qsl_rcvd_via.caseInsensitiveCompare("B") == .orderedSame) {
                        qso_record?.cards?.qsl_rcvd = "B"
                    } else if (validation.0.qsl_rcvd && validation.0.qsl_rcvd_via.caseInsensitiveCompare("D") == .orderedSame) {
                        qso_record?.cards?.qsl_rcvd = "D"
                    }
                  
                    CoreDataManager.instance.saveContext()
                   
                    
                    self.numImportedQSO += 1
                    self.qsoTotal += 1
                }
                lineBuffer.removeAll()
            } else {
                print ("Failed to import record: \(validation.0.call ), \(validation.0.date ), reason: \(validation.1)")
            }
            
        }
        let main = DispatchQueue.main
        main.async {
            if self.numImportedQSO == 0 {
                if self.numDuplicate == self.qsoTotal {
                    self.totalImported.text = ("File is already imported.")
                } else {
                    self.totalImported.text = ("Unable to import! File is empty or damaged.")
                }
                self.totalImported.textColor = UIColor.red
            } else {
                print("Done")
                self.percentageImported.text = ("100% done.")
            }
        }
    }
    
    func parseBuffer(lineBuffer:String) -> (Import, Int) {
        var values = Import()
        var isValid = 0
        /*
         0b00001 - Call OK
         0b00010 - Date OK
         0b00100 - Time OK
         0b01000 - Band OK
         0b10000 - Mode OK
         */
        let fields = lineBuffer.components(separatedBy: "<")
        for field in fields {
            let dbTag = getDBField(input: field)
            let adifValue = returnFieldValue(input: field)
            if (!(dbTag == "-") && !(adifValue == "-")) {
                
                switch (dbTag) {
                case "call":
                    values.call = adifValue.uppercased().trimmingCharacters(in: .whitespacesAndNewlines)
                    isValid |= 0b00001
                    break
                    
                case "date":
                    // "yyyy-MM-dd"
                    dateCompare = adifValue
                    dateCompare.insert("-", at: adifValue.index(adifValue.startIndex, offsetBy: 6))
                    dateCompare.insert("-", at: adifValue.index(adifValue.startIndex, offsetBy: 4))
                    
                    values.date = dateCompare
                    isValid |= 0b00010
                    break
                    
                case "time_on":
                    timeCompare = adifValue
                    
                    if (adifValue.count > 4) {
                        timeCompare.insert(":", at: timeCompare.index(timeCompare.startIndex, offsetBy: 4))
                    }
                    timeCompare.insert(":", at: timeCompare.index(timeCompare.startIndex, offsetBy: 2))
                    
                    values.time = timeCompare
                    isValid |= 0b00100
                    break
                    
                case "frequency":
                    values.freq = adifValue
                    break
                    
                case "mode":
                    values.mode = adifValue.uppercased()
                    isValid |= 0b10000
                    break
                    
                case "submode":
                    values.submode = adifValue.uppercased()
                    break
                    
                case "rst_sent":
                    values.rst_out = adifValue
                    break
                    
                case "rst_rcvd":
                    values.rst_in = adifValue
                    break
                    
                case "band":
                    values.band = adifValue.lowercased()
                    isValid |= 0b01000
                    break
                    
                case "comment":
                    values.comment = adifValue
                    break
                    
                case "gridsquare":
                    values.locator = adifValue
                    break
                    
                case "sat_name_mode":
                    values.sat_name = adifValue.trimmingCharacters(in: .whitespacesAndNewlines)
                    break
                    
                case "prop_mode":
                    values.propagation_mode = adifValue.trimmingCharacters(in: .whitespacesAndNewlines)
                    break
                    
                case "name":
                    values.name = adifValue
                    break
                    
                case "qth":
                    values.qth = adifValue
                    break
                    
                case "iota_ref":
                    values.iota_ref = adifValue
                    break
                    
                case "sota_ref":
                    values.sota_ref = adifValue
                    break
                    
                case "eqsl_qsl_sent":
                    if (adifValue.caseInsensitiveCompare("Y") == .orderedSame) {
                        values.eqsl_sent = adifValue
                    }
                    break
                    
                case "eqsl_qsl_rcvd":
                    if (adifValue.caseInsensitiveCompare("Y") == .orderedSame) {
                        values.eqsl_rcvd = adifValue
                    }
                    break
                    
                case "qsl_rcvd":
                    if (adifValue.caseInsensitiveCompare("Y") == .orderedSame) {
                        values.qsl_rcvd = true
                    }
                    break
                    
                case "qslmsg":
                    values.message = adifValue
                    break
                    
                case "qsl_sent":
                    if (adifValue.caseInsensitiveCompare("Y") == .orderedSame) {
                        values.qsl_sent = true
                    }
                    break
                    
                case "qsl_sent_via":
                    values.qsl_sent_via = adifValue
                    break
                    
                case "qsl_rcvd_via":
                    values.qsl_rcvd_via = adifValue
                    break
                    
                case "lotw_qsl_sent":
                    if (adifValue.caseInsensitiveCompare("Y") == .orderedSame) {
                        values.lotw_qsl_sent = adifValue
                    }
                    break
                    
                case "lotw_qsl_rcvd":
                    if (adifValue.caseInsensitiveCompare("Y") == .orderedSame) {
                        values.lotw_qsl_rcvd = adifValue
                    }
                    break
                case "eQSLDownload":
                    if (adifValue.caseInsensitiveCompare("eQSL.cc DownloadInBox") == .orderedSame) {
                        eQSLDownload = true
                    }
                    break
                    
                default:
                    //                    values.put(dbTag, adifValue);
                    break
                }
            }
        }
        return (values, isValid);
    }
    
    func getDBField(input:String) -> String{
        if (!input.isEmpty) {
            let valueToCheck = input.components(separatedBy: ":")[0]
            for label in self.labels {
                if(valueToCheck.caseInsensitiveCompare(label[0]) == .orderedSame) {
                    return label[1]
                }
            }
        }
        return "-"
    }
    
    func returnFieldValue(input:String) -> String{
        //       let strip = input.replacingOccurrences(of: "\\s++$", with: "")
        let strip = input.trimmingCharacters(in: .whitespaces)
        var txtValue = strip.components(separatedBy: ">")
        if ((txtValue.count > 1) && (txtValue[0].contains(":"))) {
            var tagSize = txtValue[0].components(separatedBy: ":")
            
            let varSize = Int(tagSize[1])
            if (varSize! > 0) {
                if (txtValue[1].count == varSize) {
                    return txtValue[1]
                } else {
                    if (txtValue[1].count == Int(tagSize[1])) {
                        return txtValue[1]
                    } else {
                        return "-"
                    }
                }
            }
        }
        return "-"
    }
    
    func someEntityExists(call:String, date:String, time:String) -> Bool {
        let managedContext = CoreDataManager.instance.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "QSO")
        fetchRequest.includesSubentities = false
        let predicate1 = NSPredicate(format: "call == %@", call)
        let predicate2 = NSPredicate(format: "date == %@", date)
        let predicate3 = NSPredicate(format: "time_on == %@", time)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1, predicate2, predicate3])
        
        fetchRequest.predicate = predicateCompound
        var entitiesCount = 0
        
        do {
            entitiesCount = try managedContext.count(for: fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        if entitiesCount > 0 {
            self.numDuplicate += 1
            print ("\(call) has duplicate record at \(date)")
        }
 
        return entitiesCount > 0
    }
}
