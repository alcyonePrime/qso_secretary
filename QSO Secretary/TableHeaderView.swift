//
//  HeaderView.swift
//  QSO Secretary
//
//  Created by Natalia Mykhailovska on 2018-02-21.
//  Copyright © 2018 Andrew Valender. All rights reserved.
//

import UIKit

class TableHeaderView: UIView {
    @IBOutlet var contentView: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("TableHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
